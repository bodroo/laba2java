/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package javalab2;

/**
 *
 * @author Елизавета
 */
    import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;

public class javalaba3_4 {

    public static void main(String[] args) {
        InputStream inputStream = System.in;
        Reader inputStreamReader = new InputStreamReader(inputStream);
        BufferedReader bufferedReader = new BufferedReader(inputStreamReader);

        String Ns;
        String Ms;
        int N;
        int M;
        try {
            Ns = bufferedReader.readLine();
            N = Integer.parseInt(Ns);//проверка на правильность введенных символов
        } catch (Exception ex) {
            System.out.println("Произoшла ощибка при попытке ввода N.");
            return;
        }

        try {
            Ms = bufferedReader.readLine();
            M = Integer.parseInt(Ms);//проверка на правильность ввода
        } catch (Exception ex) {
            System.out.println("Произошла ощибка при попытке ввода M.");
            return;
        }
        System.out.println(min(N, M)+ " минимальное число");

    }

    public static int min(int a, int b) {
        if (a < b) {
            return a;
        } else {
            return b;
        }

    }

}