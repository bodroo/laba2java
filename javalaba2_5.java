/*
 * ������ � ���������� ��� �����, � ���� ����� ����������, 
 * ������� ��������� ������ ����������. 
 * ���� ����� ������, �� �� ����� ����� � ������� ��������� � ������ ���� ������.
 */
package javalab2;

/**
 *
 * @author ���������
 */
import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


public class javalaba2_5 {
    public static void main(String[] args) {
        InputStream inputStream = System.in;
        Reader inputStreamReader = new InputStreamReader(inputStream);
        BufferedReader bufferedReader = new BufferedReader(inputStreamReader);

        String a, b;
        try {
            a = bufferedReader.readLine();//������ ������ � ����������
            b = bufferedReader.readLine(); //������ ������ � ����������
        } catch (Exception ex) {
            System.out.println("��������� ������ ��� ������� ����� ����.");
            return;
        }
        if (match(a) && match(b)) {
            if (a.equals(b)) {
                System.out.println("����� ���������");
            } else if (a.length() == b.length()) {
                System.out.println("����� ���� �����");
            } else System.out.println("����� �� �����");

        }
        else System.out.println("������.� ����� �� ����� �������������� �����.");

    }

    public static boolean match(String s){

        Pattern p = Pattern.compile("^[a-zA-Z�-��-�]+");//������� ���������� ���������

        Matcher m = p.matcher(s);// ����������� ������ �� ���������� � ��������

      return m.matches(); // ���������� ��������
    }
    
}